import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class FattorizazioneTestCase {

	private MathFattorizzazione classFattorizzazione;
	@Before
	public void setUp() throws Exception {
		classFattorizzazione = new MathFattorizzazione();
	}
    private List<Integer> list(int... factors) {
        List<Integer> list = new ArrayList<>();
        for (int i : factors)
            list.add(i);
        return list;
    }
	@Test
    public void testOne() {
        //we expect an empty list of factors
        assertEquals(list(), classFattorizzazione.generate(1));
    }
 
    @Test
    public void testTwo() {
        assertEquals(list(2), classFattorizzazione.generate(2));
    }



    @Test
    public void testThree() {
        assertEquals(list(3), classFattorizzazione.generate(3));
    }
    
    @Test
    public void testFour() {
        assertEquals(list(2, 2), classFattorizzazione.generate(4));
    }
    
    @Test
    public void testSix() {
        assertEquals(list(2, 3), classFattorizzazione.generate(6));
    }
    
    @Test
    public void testSeven() {
        assertEquals(list(2, 2, 2), classFattorizzazione.generate(8));
    }
    
    @Test
    public void testNine() {
        assertEquals(list(3, 3), classFattorizzazione.generate(9));
    }
    @Test
    public void testEleven(){
    	assertEquals(list(11), classFattorizzazione.generate(11));
    }
    /**/
}
