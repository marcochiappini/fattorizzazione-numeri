import java.util.ArrayList;
import java.util.List;

public class MathFattorizzazione {

	public List<Integer> generate(int number) {
		ArrayList<Integer> factors = new ArrayList<Integer>();
		number = divideNumber(number, factors);
		if (number > 0 && number != 1)
		{
			factors.add(number);
		}
		return factors;
	}
    private boolean isPrime(int number)
    {
       boolean flag = true;
       for(int i = 2; i < number ; i++){
    	   if(number % i == 0){
    		   flag = false;
    	   }
       }
       return flag;
    }
   
	private int divideNumber(int number, ArrayList<Integer> factors) {
			for(int i = 2; i < number; i++){
				while (number % i == 0 && isPrime(i)){
				factors.add(i);
				number /= i;
				}
			}
		return number;
	}

}
